Very simple chat service using Nodejs, express and socket.io.
No long term message storage (no database), saves last 50 messages as a list variable.

Running on Heroku at: https://generic-chat.herokuapp.com/
