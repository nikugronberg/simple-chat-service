"use strict"

const express = require ("express");
const app = require ("express") ();
const server = require ("http").createServer(app);
const io = require ("socket.io") (server);

const ChatHistoryLength = 50;

//setting up server
const port = process.env.PORT || 7000;
app.use("/", express.static(__dirname + "/"));

server.listen(port);
console.log("Server started, listening to port: " + port);

//routing
app.get("/", (req, res) => {
  res.sendFile(__dirname + "/main.html");
});

//socket business
io.on("connection", (socket) => {
  console.log("Connection " + socket.id);
  socket.emit("serverNewUser", {history: messageArray});


  socket.on("clientNewMessage", (data) => {
    let message = new Message(data.id, new Date(), data.name, data.content);
    newMessageToArray(message);
    console.log("New message from: " + data.id);
    io.emit("serverNewMessage", {timestamp: message.timestamp, name: message.name, content: message.content});
  });
});


//chat business
const messageArray = []

class Message {
  constructor(id, timestamp, name, content) {
    this.id = id;
    this.timestamp = timestamp;
    this.name = name;
    this.content = content;
  }
}

const newMessageToArray = (Message) => {
  messageArray.push(Message);
  if (messageArray.length > ChatHistoryLength) {
    messageArray.splice(0, 1);
  }
}
