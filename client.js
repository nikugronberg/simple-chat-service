"use strict";

$(document).ready(() => {

  //resetting texts boxes and such
  $("#chat").val("Welcome back!");
  $("#message").val("");
  $("#message").focus();
  $("#name").val("");

  //connect to the server
  const socket = io();
  socket.on("connect", (req, res) => {
    //do nothing
  });

  //receive message from server, add it to the chat window
  socket.on("serverNewMessage", (data) => {
    let chat = $("#chat").val();
    chat += "\n" + formatMessage(data.timestamp, data.name, data.content);
    $("#chat").val(chat);
    $("#chat").scrollTop($("#chat")[0].scrollHeight);
  });

  //receive this when making connection first time to server, get chat
  socket.on("serverNewUser", (data) => {
    let chatHistory = "";
    data.history.forEach((message) => {
      chatHistory += "\n" + formatMessage(message.timestamp, message.name, message.content);
    });
    let text = $("#chat").val();
    $("#chat").val(text + chatHistory);
    $("#chat").scrollTop($("#chat")[0].scrollHeight);
  });

  //send message
  $("#send").click(() => {
    let text = $("#message").val().trim()
    let name = $("#name").val().trim();
    if(text.length == 0 || name.length == 0) return;
    $("#message").val("");
    socket.emit("clientNewMessage", {id: socket.id, name: name, content: text});
    $("#message").focus();
  });

  //send message with enter
  $(document).on("keypress", (e) => {
    if(e.keyCode == "13") {
      $("#send").click();
    }
  });

});

//handy function for formatting messages
const formatMessage = (timestamp, name, content) => {
  timestamp = new Date(timestamp);
  let time = timestamp.getDate() + "." + (timestamp.getMonth()+1) + "." + timestamp.getFullYear() + " " + timestamp.getHours() + ":" + timestamp.getMinutes();
  return time + " " + name + ": " + content
}
